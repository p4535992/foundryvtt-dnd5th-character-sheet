/**
 * This is your TypeScript entry file for Foundry VTT.
 * Register custom settings, sheets, and constants using the Foundry API.
 * Change this heading to be more descriptive to your module, or remove it.
 * Author: [your name]
 * Content License: [copyright and-or license] If using an existing system
 * 					you may want to put a (link to a) license or copyright
 * 					notice here (e.g. the OGL).
 * Software License: [your license] Put your desired license here, which
 * 					 determines how others may use and modify your module
 */

// Import TypeScript modules
import { registerSettings } from './module/settings.js';
import { preloadTemplates } from './module/preloadTemplates.js';
import { initHooks } from './module/Hooks';

import { MODULE_ID } from './module/constants';
//@ts-ignore
import { DND5E } from "../../systems/dnd5e/module/config.js";
//@ts-ignore
import ActorSheet5e from "../../systems/dnd5e/module/actor/sheets/base.js";
//@ts-ignore
import ActorSheet5eNPC from "../../systems/dnd5e/module/actor/sheets/npc.js";
//@ts-ignore
import ActorSheet5eCharacter from "../../systems/dnd5e/module/actor/sheets/character.js";

// handlebar helper compare string
Handlebars.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});

export let debugEnabled = 0;
// 0 = none, warnings = 1, debug = 2, all = 3
export let debug = (...args) => {if (debugEnabled > 1) console.log(`DEBUG: ${MODULE_ID} | `, ...args)};
export let log = (...args) => console.log(`${MODULE_ID} | `, ...args);
export let warn = (...args) => {if (debugEnabled > 0) console.warn(`${MODULE_ID} | `, ...args)};
export let error = (...args) => console.error(`${MODULE_ID} | `, ...args)
export let i18n = key => {
  return game.i18n.localize(key);
};
export let setDebugLevel = (debugText: string) => {
  debugEnabled = {"none": 0, "warn": 1, "debug": 2, "all": 3}[debugText] || 0;
  // 0 = none, warnings = 1, debug = 2, all = 3
  CONFIG.debug.hooks = debugEnabled >= 3;
}

export class FoundryVTTDnD5thCharacterSheet extends ActorSheet5eCharacter {

	get template() {
		warn("get template " + this);
		return `modules/${MODULE_ID}/templates/actors/DND5e.html`;
	}
  
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			classes: ["foundryVTTDnD5thCharacterSheet", "dnd5e", "sheet", "actor", "character"],
			blockFavTab: true,
			width: 740,
			height: 720
		});
	}

	_createEditor(target, editorOptions, initialContent) {
		editorOptions.min_height = 200;
		super._createEditor(target, editorOptions, initialContent);
	}

	// save all simultaneously open editor field when one field is saved
	async _onEditorSave(target, element, content) {
		  //return this.submit();
		  super._onEditorSave();
	}
	  
	getData() {
		const sheetData = super.getData();
	
		warn('sheetData', sheetData);
		warn('actor', this);

	}
}

// Register
Actors.registerSheet("dnd5e", FoundryVTTDnD5thCharacterSheet, {
	types: ["character"],
	makeDefault: false
});

Hooks.on("renderFoundryVTTDnD5thCharacterSheet", (app, html, data) => {
	warn("FoundryVTT DnD5th Character Sheet rendered!");
});

/* ------------------------------------ */
/* Initialize module					*/
/* ------------------------------------ */
Hooks.once('init', async function() {
	warn(`Initializing ${MODULE_ID}`);

	initHooks();

	// Assign custom classes and constants here

	
	// Register custom module settings
	registerSettings();
	
	// Preload Handlebars templates
	await preloadTemplates();

	// Register custom sheets (if any)
});

/* ------------------------------------ */
/* Setup module							*/
/* ------------------------------------ */
Hooks.once('setup', function() {
	// Do anything after initialization but before ready
	//setupModules();
	registerSettings();
});

/* ------------------------------------ */
/* When ready							*/
/* ------------------------------------ */
Hooks.once('ready', function() {
	// Do anything once the module is ready
	warn("FoundryVTT DnD5th Character Sheet is ready!");
});

// Add any additional hooks if necessary
