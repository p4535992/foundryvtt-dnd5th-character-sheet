import { MODULE_ID } from '../module/constants.js';

export const preloadTemplates = async function() {
	const templatePaths = [
		// Add paths to "modules/foundryvtt-dnd5th-character-sheet/templates"
		`modules/${MODULE_ID}/templates/actors/DND5e.html`,
	];

	return loadTemplates(templatePaths);
}
