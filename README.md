THIS PROJECT IS NOT WORKING AND IT ILL NEVER BE IT REMAIN JUST LIKE A REMAINDER
===========================================================================================

# foundryvtt-dnd5e-characther-sheet

A tentative to make a DnD5th character sheet for foundryvvt similar to the official of Wizard of the Coast

## First Styling

A big of the work is regain from the post [HTML Character Sheet by ozay34](https://www.reddit.com/r/DnD/comments/dm4xj2/html_character_sheet/) the project is here [HTMLCharacterSheets](https://github.com/adw4236/HTMLCharacterSheets) preview of the html version on [here](https://drive.google.com/file/d/1rNZmhkAtNjbyxQ1wcU8ZcMqtzDmVwDnw/view)

## First Hand

Shamlessly taking ideas (and some code) from both the [DnDBeyond character sheet](https://github.com/jopeek/fvtt-dndbeyond-character-sheet) and the [Tidy 5e character sheet](https://github.com/sdenec/tidy5e-sheet).


## First Impression

![img1](./readme-img/Image 1.png)

![img2](./readme-img/Image 2.png)

![img3](./readme-img/Image 3.png)